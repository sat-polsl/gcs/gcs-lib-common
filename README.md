## Building CPP source for protocol buffers

Example:
    - https://github.com/grpc/grpc/blob/master/src/cpp/README.md#cmake

## Notes:

If Golang.CI is failing on MacOS with message similar to:

```
Can't run linter [...]: goimports: can't extract issues from gofmt diff output [...]
```

You need to install GNU Diff, and force OS to prefer it over Apple Diff:

```
brew install diffutils
export PATH=/opt/homebrew/bin:${PATH}
```