/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package fake

import (
	"context"
	"fmt"

	"go.opentelemetry.io/otel"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io"
)

var (
	tracer = otel.Tracer("gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io/zmq")
)

type Sender struct {
	dialed bool
}

var _ io.Sender = (*Sender)(nil)

func (s *Sender) Dial(e string) error {
	if e == "" {
		return fmt.Errorf("endpoint empty")
	}

	s.dialed = true
	return nil
}

func (s *Sender) Send(ctx context.Context, msg io.Message) error {
	_, span := tracer.Start(ctx, "Send")
	defer span.End()

	if !s.dialed {
		return fmt.Errorf("not dialed")
	}
	return nil
}
