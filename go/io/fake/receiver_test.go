/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package fake_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io/fake"
)

type ReceiverTestSuite struct {
	suite.Suite
}

func TestReceiverTestSuite(t *testing.T) {
	suite.Run(t, new(ReceiverTestSuite))
}

func (s *ReceiverTestSuite) TestDial() {
	x := &fake.Receiver{}
	err := x.Dial("endpoint")
	assert.NoError(s.T(), err)
}

func (s *ReceiverTestSuite) TestDial_Error() {
	x := &fake.Receiver{}
	err := x.Dial("")
	require.Error(s.T(), err)
}

func (s *ReceiverTestSuite) TestRecv() {
	x := &fake.Receiver{}
	err := x.Dial("endpoint")
	require.NoError(s.T(), err)

	_, err = x.Recv()
	assert.NoError(s.T(), err)
}

func (s *ReceiverTestSuite) TestSend_Error() {
	x := &fake.Receiver{}
	_, err := x.Recv()
	assert.Error(s.T(), err)
}
