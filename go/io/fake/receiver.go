/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package fake

import (
	"fmt"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io"
)

type Receiver struct {
	dialed bool
	gen    func() (io.Message, error)
}

var _ io.Receiver = (*Receiver)(nil)

func (r *Receiver) Dial(e string) error {
	if e == "" {
		return fmt.Errorf("endpoint empty")
	}

	r.dialed = true
	return nil
}

func (r *Receiver) SetOption(string, interface{}) error {
	return nil
}

func (r *Receiver) Recv() (io.Message, error) {
	if !r.dialed {
		return Message{}, fmt.Errorf("not dialed")
	}

	if r.gen != nil {
		return r.gen()
	}

	return Message{}, nil
}
