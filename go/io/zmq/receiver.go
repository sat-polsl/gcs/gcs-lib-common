/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package zmq

import (
	"context"
	"sync"

	"github.com/go-zeromq/zmq4"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io"
)

type Sub struct {
	topics sync.Map
	socket zmq4.Socket
}

var _ io.Receiver = (*Sub)(nil)

func NewSub(ctx context.Context, socket zmq4.Socket) *Sub {
	return &Sub{
		socket: socket,
	}
}

func (s *Sub) Dial(endpoint string) error {
	return s.socket.Dial(endpoint)
}

func (s *Sub) SetOption(name string, value interface{}) error {
	if val, ok := s.topics.Load(name); ok && val == value {
		return nil
	}

	s.topics.Store(name, value)

	return s.socket.SetOption(name, value)
}

func (s *Sub) GetOption(name string) (interface{}, error) {
	return s.socket.GetOption(name)
}

func (s *Sub) Recv() (io.Message, error) {
	msg, err := s.socket.Recv()
	if err != nil {
		return Msg{}, err
	}

	if msg.Err() != nil {
		return Msg{}, msg.Err()
	}

	if len(msg.Frames) < 2 {
		return Msg{}, ErrInvalidMsg
	}

	return Msg(msg), nil
}
