/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package zmq

import (
	"context"

	"github.com/go-zeromq/zmq4"
	"go.opentelemetry.io/otel"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io"
)

var (
	tracer = otel.Tracer("gitlab.com/sat-polsl/gcs/gcs-lib-common/go/io/zmq")
)

type Pub struct {
	socket zmq4.Socket
}

var _ io.Sender = (*Pub)(nil)

func NewPub(ctx context.Context, socket zmq4.Socket) *Pub {
	return &Pub{
		socket: socket,
	}
}

func (p *Pub) Dial(endpoint string) error {
	return p.socket.Dial(endpoint)
}

func (p *Pub) Send(ctx context.Context, msg io.Message) error {
	_, span := tracer.Start(ctx, "Send")
	defer span.End()

	if len(msg.Topic()) == 0 {
		return ErrInvalidTopic
	}

	zmtp := zmq4.Msg{
		Frames: append([][]byte{}, msg.Topic(), msg.Data()),
	}

	return p.socket.Send(zmtp)
}
