/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package io

import (
	"context"
	"fmt"
	"reflect"
	"runtime"
	"strings"
	"sync"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/metric/global"
)

type Client struct {
	receiver    Receiver
	mu          sync.RWMutex
	handlers    map[string]Handler
	middlewares []Middleware
}

type Option func(*Client)

func NewClient(options ...Option) *Client {
	c := &Client{}

	for _, opt := range options {
		opt(c)
	}

	return c
}

func WithReceiver(r Receiver) Option {
	return func(c *Client) {
		c.receiver = r
	}
}

func WithTopicHandler(t string, h Handler) Option {
	return func(c *Client) {
		c.HandleFunc(t, h)
	}
}

func WithMiddleware(mw Middleware) Option {
	return func(c *Client) {
		c.Use(mw)
	}
}

func decorateMiddleware(m Middleware) (Middleware, error) {
	name := runtime.FuncForPC(reflect.ValueOf(m).Pointer()).Name()

	tracer := otel.Tracer("middleware_tracer_" + name)
	counter, err := global.Meter("middleware_counter_" + name).SyncInt64().Counter("")
	if err != nil {
		return nil, err
	}

	return func(ctx context.Context, msg Message) {
		counter.Add(ctx, 1)
		ctx, span := tracer.Start(ctx, "middleware")
		defer span.End()
		m(ctx, msg)
	}, nil
}

func decorateHandler(h Handler) (Handler, error) {
	name := runtime.FuncForPC(reflect.ValueOf(h).Pointer()).Name()

	tracer := otel.Tracer("handler_tracer_" + name)
	counter, err := global.Meter("handler_counter_" + name).SyncInt64().Counter("")
	if err != nil {
		return nil, err
	}

	return func(ctx context.Context, msg Message) {
		counter.Add(ctx, 1)
		ctx, span := tracer.Start(ctx, "start_"+name)
		defer span.End()
		h(ctx, msg)
	}, nil
}

func (x *Client) Use(mwf ...Middleware) {
	x.mu.Lock()
	defer x.mu.Unlock()

	for _, mw := range mwf {
		if mw == nil {
			panic("io: nil middleware")
		}
		dmw, err := decorateMiddleware(mw)
		if err != nil {
			panic("io: failed to decorate middleware")
		}
		x.middlewares = append(x.middlewares, dmw)
	}
}

func (x *Client) HandleFunc(topic string, handler Handler) {
	x.mu.Lock()
	defer x.mu.Unlock()

	if topic == "" {
		panic("io: invalid topic")
	}
	if handler == nil {
		panic("io: nil handler")
	}

	if x.handlers == nil {
		x.handlers = make(map[string]Handler)
	}

	if _, exist := x.handlers[topic]; exist {
		panic("io: multiple registrations for " + topic)
	}

	dh, err := decorateHandler(handler)
	if err != nil {
		panic("io: failed to decorate handler")
	}

	x.handlers[topic] = dh
}

func (x *Client) ReceiveAndHandle(ctx context.Context, endpoint string) error {
	if x.receiver == nil {
		return fmt.Errorf("receiver not set")
	}

	if !strings.HasPrefix(endpoint, "tcp://") {
		endpoint = "tcp://" + endpoint
	}

	err := x.receiver.Dial(endpoint)
	if err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			m, err := x.receiver.Recv()
			if err != nil {
				return err
			}

			go func(msg Message) {
				x.mu.RLock()
				for _, mw := range x.middlewares {
					mw(ctx, msg)
				}
				x.mu.RUnlock()

				if f, exist := x.handlers[string(msg.Topic())]; exist {
					f(ctx, msg)
				}
			}(m.Clone())
		}
	}
}
