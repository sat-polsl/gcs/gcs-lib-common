/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package io

import (
	"context"
)

type Message interface {
	Topic() []byte
	Data() []byte
	Clone() Message
}

// Dialer interface is our dialer abstraction that makes zmq.Socket an implementation detail.
type Dialer interface {
	Dial(string) error
}

// Receiver interface is used to abstract zmq.Socket with functions that we will be using
// to receive data from ZeroMQ Proxy.
type Receiver interface {
	Dialer
	Recv() (Message, error)
	SetOption(string, interface{}) error
}

// Sender interface is used to abstract zmq.Socket with functions that we will be using
// to send data to ZeroMQ Proxy.
type Sender interface {
	Dialer
	Send(context.Context, Message) error
}

// Middleware is function with specific signature used to modify ZeroMQ frames.
type Middleware func(context.Context, Message)

// Handler is function with specific signature used to handle ZeroMQ frames.
type Handler func(context.Context, Message)
