time: {{- .Time }}
{{- if .Created -}}
created:
  mission: {{ .Created.NewMission.Id }}
  name: {{ .Created.NewMission.Name }}
  description: {{ .Created.NewMission.Description }}
{{- else if .Updated -}}
updated:
  mission: {{ .Updated.NewMission.Id }}
  name: {{ .Updated.NewMission.Name }}
  description: {{ .Updated.NewMission.Description }}
{{- else if .Deleted -}}
deleted:
  mission: {{ .Deleted.ID }}
{{- end -}}