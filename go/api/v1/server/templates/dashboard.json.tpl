{
    "__inputs": [
      {
        "name": "DS_POSTGRESQL",
        "label": "PostgreSQL",
        "description": "",
        "type": "datasource",
        "pluginId": "postgres",
        "pluginName": "PostgreSQL"
      }
    ],
    "__elements": {},
    "__requires": [
      {
        "type": "panel",
        "id": "geomap",
        "name": "Geomap",
        "version": ""
      },
      {
        "type": "grafana",
        "id": "grafana",
        "name": "Grafana",
        "version": "9.4.3"
      },
      {
        "type": "datasource",
        "id": "postgres",
        "name": "PostgreSQL",
        "version": "1.0.0"
      },
      {
        "type": "panel",
        "id": "timeseries",
        "name": "Time series",
        "version": ""
      }
    ],
    "annotations": {
      "list": [
        {
          "builtIn": 1,
          "datasource": {
            "type": "grafana",
            "uid": "-- Grafana --"
          },
          "enable": true,
          "hide": true,
          "iconColor": "rgba(0, 211, 255, 1)",
          "name": "Annotations & Alerts",
          "target": {
            "limit": 100,
            "matchAny": false,
            "tags": [],
            "type": "dashboard"
          },
          "type": "dashboard"
        }
      ]
    },
    "editable": true,
    "fiscalYearStartMonth": 0,
    "graphTooltip": 0,
    "id": null,
    "links": [],
    "liveNow": false,
    "panels": [
      {
        "datasource": {
          "type": "postgres",
          "uid": "${DS_POSTGRESQL}"
        },
        "fieldConfig": {
          "defaults": {
            "color": {
              "mode": "thresholds"
            },
            "custom": {
              "hideFrom": {
                "legend": false,
                "tooltip": false,
                "viz": false
              }
            },
            "mappings": [],
            "thresholds": {
              "mode": "absolute",
              "steps": [
                {
                  "color": "green",
                  "value": null
                },
                {
                  "color": "red",
                  "value": 80
                }
              ]
            }
          },
          "overrides": []
        },
        "gridPos": {
          "h": 15,
          "w": 12,
          "x": 0,
          "y": 0
        },
        "id": 2,
        "interval": "1s",
        "maxDataPoints": 10000,
        "options": {
          "basemap": {
            "config": {},
            "name": "Layer 0",
            "type": "default"
          },
          "controls": {
            "mouseWheelZoom": true,
            "showAttribution": true,
            "showDebug": false,
            "showMeasure": false,
            "showScale": false,
            "showZoom": true
          },
          "layers": [
            {
              "config": {
                "showLegend": true,
                "style": {
                  "color": {
                    "fixed": "dark-green"
                  },
                  "opacity": 0.4,
                  "rotation": {
                    "fixed": 0,
                    "max": 360,
                    "min": -360,
                    "mode": "mod"
                  },
                  "size": {
                    "fixed": 5,
                    "max": 15,
                    "min": 2
                  },
                  "symbol": {
                    "fixed": "img/icons/marker/circle.svg",
                    "mode": "fixed"
                  },
                  "textConfig": {
                    "fontSize": 12,
                    "offsetX": 0,
                    "offsetY": 0,
                    "textAlign": "center",
                    "textBaseline": "middle"
                  }
                }
              },
              "location": {
                "mode": "auto"
              },
              "name": "Layer 1",
              "tooltip": true,
              "type": "markers"
            }
          ],
          "tooltip": {
            "mode": "details"
          },
          "view": {
            "allLayers": true,
            "id": "fit",
            "lat": 40.078827,
            "lon": 9.598535,
            "padding": 30,
            "zoom": 15
          }
        },
        "pluginVersion": "9.4.3",
        "targets": [
          {
            "datasource": {
              "type": "postgres",
              "uid": "${DS_POSTGRESQL}"
            },
            "editorMode": "builder",
            "format": "table",
            "rawSql": "SELECT spacecraft_id, longitude, latitude, altitude, \"timestamp\" FROM {{ .TableName }} LIMIT 50 ",
            "refId": "A",
            "sql": {
              "columns": [
                {
                  "parameters": [
                    {
                      "name": "spacecraft_id",
                      "type": "functionParameter"
                    }
                  ],
                  "type": "function"
                },
                {
                  "parameters": [
                    {
                      "name": "longitude",
                      "type": "functionParameter"
                    }
                  ],
                  "type": "function"
                },
                {
                  "parameters": [
                    {
                      "name": "latitude",
                      "type": "functionParameter"
                    }
                  ],
                  "type": "function"
                },
                {
                  "parameters": [
                    {
                      "name": "altitude",
                      "type": "functionParameter"
                    }
                  ],
                  "type": "function"
                },
                {
                  "parameters": [
                    {
                      "name": "\"timestamp\"",
                      "type": "functionParameter"
                    }
                  ],
                  "type": "function"
                }
              ],
              "groupBy": [
                {
                  "property": {
                    "type": "string"
                  },
                  "type": "groupBy"
                }
              ],
              "limit": 50
            },
            "table": "{{ .TableName }}"
          }
        ],
        "title": "Map",
        "type": "geomap"
      },
      {
        "datasource": {
          "type": "postgres",
          "uid": "${DS_POSTGRESQL}"
        },
        "fieldConfig": {
          "defaults": {
            "color": {
              "mode": "palette-classic"
            },
            "custom": {
              "axisCenteredZero": false,
              "axisColorMode": "text",
              "axisLabel": "",
              "axisPlacement": "auto",
              "axisSoftMin": 60,
              "barAlignment": 0,
              "drawStyle": "line",
              "fillOpacity": 0,
              "gradientMode": "none",
              "hideFrom": {
                "legend": false,
                "tooltip": false,
                "viz": false
              },
              "lineInterpolation": "linear",
              "lineWidth": 1,
              "pointSize": 5,
              "scaleDistribution": {
                "type": "linear"
              },
              "showPoints": "auto",
              "spanNulls": false,
              "stacking": {
                "group": "A",
                "mode": "none"
              },
              "thresholdsStyle": {
                "mode": "off"
              }
            },
            "mappings": [],
            "thresholds": {
              "mode": "absolute",
              "steps": [
                {
                  "color": "green",
                  "value": null
                },
                {
                  "color": "red",
                  "value": 80
                }
              ]
            }
          },
          "overrides": []
        },
        "gridPos": {
          "h": 8,
          "w": 12,
          "x": 12,
          "y": 0
        },
        "id": 4,
        "maxDataPoints": 10000,
        "options": {
          "legend": {
            "calcs": [],
            "displayMode": "list",
            "placement": "bottom",
            "showLegend": true
          },
          "tooltip": {
            "mode": "single",
            "sort": "none"
          }
        },
        "targets": [
          {
            "datasource": {
              "type": "postgres",
              "uid": "${DS_POSTGRESQL}"
            },
            "editorMode": "builder",
            "format": "table",
            "rawSql": "SELECT altitude, \"timestamp\" FROM {{ .TableName }} LIMIT 50 ",
            "refId": "A",
            "sql": {
              "columns": [
                {
                  "parameters": [
                    {
                      "name": "altitude",
                      "type": "functionParameter"
                    }
                  ],
                  "type": "function"
                },
                {
                  "parameters": [
                    {
                      "name": "\"timestamp\"",
                      "type": "functionParameter"
                    }
                  ],
                  "type": "function"
                }
              ],
              "groupBy": [
                {
                  "property": {
                    "type": "string"
                  },
                  "type": "groupBy"
                }
              ],
              "limit": 50
            },
            "table": "{{ .TableName }}"
          }
        ],
        "title": "Altitude",
        "type": "timeseries"
      }
    ],
    "refresh": "",
    "revision": 1,
    "schemaVersion": 38,
    "style": "dark",
    "tags": [],
    "templating": {
      "list": []
    },
    "time": {
      "from": "2023-01-19T18:29:41.869Z",
      "to": "2023-01-19T18:30:32.205Z"
    },
    "timepicker": {},
    "timezone": "",
    "title": "Mock Dashboard",
    "uid": "M-CbdHoVz",
    "version": 2,
    "weekStart": ""
  }