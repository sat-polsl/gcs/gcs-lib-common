/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package server_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1/server"
	grpcFake "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/grpc/fake"
	storageFake "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/fake"
)

type ServerTestSuite struct {
	suite.Suite

	APIv1Server apiv1.StrictServerInterface
	ctx         context.Context
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(ServerTestSuite))
}

func (s *ServerTestSuite) SetupSuite() {
	s.APIv1Server = &server.Server{
		Storage:      &storageFake.Client{},
		Configurator: &grpcFake.Client{},
	}
}

func (s *ServerTestSuite) SetupTest() {
	s.ctx = context.Background()
}

func (s *ServerTestSuite) TestGetMissions() {
	_, err := s.APIv1Server.GetMissions(s.ctx, apiv1.GetMissionsRequestObject{})
	require.NoError(s.T(), err)
}

func (s *ServerTestSuite) TestCreateMission() {
	_, err := s.APIv1Server.CreateMission(s.ctx, apiv1.CreateMissionRequestObject{
		Body: &apiv1.Mission{},
	})
	require.NoError(s.T(), err)
}

func (s *ServerTestSuite) TestUpdateMissionId() {
	_, err := s.APIv1Server.UpdateMission(s.ctx, apiv1.UpdateMissionRequestObject{
		Id:   "1",
		Body: &apiv1.Mission{},
	})
	require.NoError(s.T(), err)
}

func (s *ServerTestSuite) TestGetMissionId() {
	_, err := s.APIv1Server.GetMission(s.ctx, apiv1.GetMissionRequestObject{Id: "1"})
	require.NoError(s.T(), err)
}

// func (s *ServerTestSuite) TestDeleteMissionId() {
// 	_, err := s.APIv1Server.DeleteMission(s.ctx, apiv1.DeleteMissionRequestObject{
// 		Id: "1",
// 	})
// 	require.NoError(s.T(), err)
// }

// func (s *ServerTestSuite) TestGetGrafanaTemplate() {
// 	s.APIv1Server.GetGrafanaTemplate(s.fakeResponseWriter, nil, "1")
// }
