/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package server

import (
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
)

// getOptions is a struct that implements storage.GetOptions interface
type getOptions struct {
	limit   *int
	offset  *int
	columns []string
	filters []storage.Filter
}

var _ storage.GetOptions = (*getOptions)(nil)

func (o *getOptions) Columns() ([]string, bool)         { return o.columns, len(o.columns) > 0 }
func (o *getOptions) Filters() ([]storage.Filter, bool) { return o.filters, false }

func (o *getOptions) Limit() (int, bool) {
	if o.limit == nil {
		return 0, false
	}
	return *o.limit, true
}

func (o *getOptions) Offset() (int, bool) {
	if o.offset == nil {
		return 0, false
	}
	return *o.offset, true
}

// filter is a struct that implements storage.Filter interface
type filter struct {
	field    string
	operator string
	value    interface{}
}

var _ storage.Filter = (*filter)(nil)

func (f *filter) Field() string      { return f.field }
func (f *filter) Operator() string   { return f.operator }
func (f *filter) Value() interface{} { return f.value }

// createOptions is a struct that implements storage.CreateOptions interface
type createOptions struct {
	FieldsList []storage.Field `json:"fields"`
}

var _ storage.CreateOptions = (*createOptions)(nil)

func (o *createOptions) Fields() []storage.Field {
	return o.FieldsList
}

// updateOpts is a struct that implements storage.UpdateOptions interface
type updateOpts struct {
	filters []storage.Filter
}

var _ storage.UpdateOptions = (*updateOpts)(nil)

func (o *updateOpts) Filters() ([]storage.Filter, bool) { return o.filters, false }

// deleteOptions is a struct that implements storage.DeleteOptions interface
type deleteOptions struct {
	filters []storage.Filter
}

func (o *deleteOptions) Filters() ([]storage.Filter, bool) { return o.filters, false }

var _ storage.DeleteOptions = (*deleteOptions)(nil)
