package server_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1/server"
)

func TestMessage(t *testing.T) {
	description := "test"

	for _, tc := range []server.Message{
		{
			Time: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			Updated: &server.Updated{
				NewMission: &apiv1.Mission{
					Id:          1,
					Name:        "test",
					Description: &description,
					Fields:      apiv1.Fields(`[{"name":"test","type":"string"}]`),
				},
			},
		},
		{
			Time: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			Created: &server.Created{
				NewMission: &apiv1.Mission{
					Id:          1,
					Name:        "test",
					Description: &description,
				},
			},
		},
		{
			Time: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
			Deleted: &server.Deleted{
				ID: "1",
			},
		},
	} {
		assert.NotEmpty(t, tc.String())
	}
}
