/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package server

import (
	"bytes"
	"text/template"
	"time"

	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
)

type Message struct {
	Time    time.Time
	Created *Created
	Updated *Updated
	Deleted *Deleted
}

func mCreated(m *apiv1.Mission) *Message {
	return &Message{
		Created: &Created{
			NewMission: m,
		},
	}
}

func mUpdated(m *apiv1.Mission) *Message {
	return &Message{
		Updated: &Updated{
			NewMission: m,
		},
	}
}

func mDeleted(id string) *Message {
	return &Message{
		Deleted: &Deleted{
			ID: id,
		},
	}
}

func (m *Message) String() string {
	m.Time = time.Now()
	tpl, err := template.New("message").Parse(MessageTemplate)
	if err != nil {
		panic(err)
	}

	var buffer bytes.Buffer
	if err := tpl.Execute(&buffer, m); err != nil {
		panic(err)
	}

	return buffer.String()
}

type Created struct {
	NewMission *apiv1.Mission
}

type Updated struct {
	NewMission *apiv1.Mission
}

type Deleted struct {
	ID string
}
