/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package server

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"text/template"

	"github.com/containrrr/shoutrrr/pkg/router"
	"github.com/go-zeromq/zmq4"
	"github.com/rs/zerolog/log"
	apiv1 "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/api/v1"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/grpc"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
)

type Server struct {
	Storage      storage.Storage
	Configurator grpc.ConfiguratorClient
	Sender       *router.ServiceRouter
}

var _ apiv1.StrictServerInterface = (*Server)(nil)

// GetMissions is used to get all missions
// (GET /apiv1/missions)
func (s *Server) GetMissions(ctx context.Context, request apiv1.GetMissionsRequestObject) (apiv1.GetMissionsResponseObject, error) {
	options := &getOptions{
		columns: []string{
			"id",
			"name",
			"description",
			"fields",
		},
		offset: request.Params.Offset,
		limit:  request.Params.Limit,
	}

	data, err := s.Storage.Get(ctx, "missions", options)
	if err != nil {
		log.Error().Err(err).Msg("failed to get missions from storage")

		return apiv1.GetMissions500JSONResponse{
			Reason: err.Error(),
			Code:   StorageCommunicationError,
		}, nil
	}

	res := apiv1.GetMissions200JSONResponse{}
	for _, row := range data {
		mission, err := mapToMission(row)
		if err != nil {
			log.Error().Err(err).Msg("failed to convert mission response")

			return apiv1.GetMissions500JSONResponse{
				Reason: err.Error(),
				Code:   StorageCommunicationError,
			}, nil
		}
		res = append(res, mission)
	}

	return res, nil
}

// CreateMission is used to create a new mission
// (POST /apiv1/missions)
func (s *Server) CreateMission(ctx context.Context, request apiv1.CreateMissionRequestObject) (apiv1.CreateMissionResponseObject, error) {
	log.Trace().Caller().Msg("create mission request")

	sf := []storage.Field{}
	err := json.Unmarshal([]byte(request.Body.Fields), &sf)
	if err != nil {
		log.Error().Err(err).Msg("failed to unmarshal fields")

		return apiv1.CreateMission400JSONResponse{
			Message: err.Error(),
		}, nil
	}

	if len(sf) == 0 {
		log.Error().Msg("no fields provided")

		return apiv1.CreateMission400JSONResponse{
			Message: "no fields provided",
		}, nil
	}

	m, err := structToMap(request.Body) // convert to format accepted by storage
	if err != nil {
		log.Error().Err(err).Msg("failed to convert mission to map")

		return apiv1.CreateMission500JSONResponse{
			Reason: err.Error(),
			Code:   ConversionError,
		}, nil
	}

	err = s.Storage.Insert(ctx, "missions", m)
	if err != nil {
		log.Error().Err(err).Msg("failed to insert mission in storage")

		return apiv1.CreateMission500JSONResponse{
			Reason: err.Error(),
			Code:   StorageCommunicationError,
		}, nil
	}

	opts := createOptions{
		FieldsList: sf,
	}

	err = s.Storage.Create(ctx, fmt.Sprintf("spacecraft_%d", request.Body.Id), &opts)
	if err != nil {
		log.Error().Err(err).Msg("failed to create mission in storage")

		return apiv1.CreateMission400JSONResponse{
			Message: err.Error(),
		}, nil
	}

	_, err = s.Configurator.Set(ctx, &grpc.SetRequest{
		Option: &grpc.Option{
			Name:  zmq4.CmdSubscribe,
			Value: fmt.Sprintf("spacecraft_%d", request.Body.Id),
		},
	})
	if err != nil {
		log.Error().Err(err).Msg("failed to subscribe to spacecraft")
		return apiv1.CreateMission500JSONResponse{
			Reason: err.Error(),
			Code:   GRPCCommunicationError,
		}, nil
	}

	errs := s.Sender.Send(mCreated(request.Body).String(), nil)
	if errs != nil {
		go logWarn(errs)
	}

	return apiv1.CreateMission201JSONResponse(*request.Body), nil
}

// UpdateMission is used to delete a mission by id
// (PUT /apiv1/missions/{id})
func (s *Server) UpdateMission(ctx context.Context, request apiv1.UpdateMissionRequestObject) (apiv1.UpdateMissionResponseObject, error) {
	sf := []storage.Field{}
	err := json.Unmarshal([]byte(request.Body.Fields), &sf)
	if err != nil {
		log.Error().Err(err).Msg("failed to unmarshal fields")

		return apiv1.UpdateMission400JSONResponse{
			Message: err.Error(),
		}, nil
	}

	m, err := structToMap(request.Body) // convert to format accepted by storage
	if err != nil {
		log.Error().Err(err).Msg("failed to convert mission to map")

		return apiv1.UpdateMission500JSONResponse{
			Reason: err.Error(),
			Code:   ConversionError,
		}, nil
	}

	opts := &updateOpts{
		filters: []storage.Filter{
			&filter{
				field:    "id",
				value:    request.Id,
				operator: "=",
			},
		},
	}

	err = s.Storage.Update(ctx, "missions", m, opts)
	if err != nil {
		log.Error().Err(err).Msg("failed to create mission in storage")

		return apiv1.UpdateMission500JSONResponse{
			Reason: err.Error(),
			Code:   StorageCommunicationError,
		}, nil
	}

	errs := s.Sender.Send(mUpdated(request.Body).String(), nil)
	if errs != nil {
		go logWarn(errs)
	}

	return apiv1.UpdateMission200JSONResponse{}, nil
}

// DeleteMission is used to delete a mission by id
// (DELETE /apiv1/missions/{id})
func (s *Server) DeleteMission(ctx context.Context, request apiv1.DeleteMissionRequestObject) (apiv1.DeleteMissionResponseObject, error) {
	panic("unimplemented")

	//nolint:govet
	errs := s.Sender.Send(mDeleted(request.Id).String(), nil)
	if errs != nil {
		go logWarn(errs)
	}

	return apiv1.DeleteMission204Response{}, nil
}

// Get a mission by id
// (GET /apiv1/missions/{id})
func (s *Server) GetMission(ctx context.Context, request apiv1.GetMissionRequestObject) (apiv1.GetMissionResponseObject, error) {
	missions, err := s.Storage.Get(ctx, "missions", &getOptions{
		columns: []string{
			"description",
			"fields",
			"id",
			"name",
		},
		filters: []storage.Filter{
			&filter{
				field:    "id",
				operator: "=",
				value:    request.Id,
			},
		},
	})
	if err != nil {
		log.Error().Err(err).Msg("failed to get mission from storage")

		return apiv1.GetMission500JSONResponse{
			Reason: err.Error(),
			Code:   StorageCommunicationError,
		}, nil
	}

	if len(missions) == 0 {
		return apiv1.GetMission404JSONResponse{
			Message: "mission not found",
		}, nil
	}

	m, err := mapToMission(missions[0])
	if err != nil {
		log.Error().Err(err).Msg("failed to convert mission response")

		return apiv1.GetMission500JSONResponse{
			Reason: err.Error(),
			Code:   ConversionError,
		}, nil
	}

	return apiv1.GetMission200JSONResponse(m), nil
}

// Get a grafana template for a mission by id
// (GET /apiv1/missions/{id}/grafana)
func (s *Server) GetGrafanaTemplate(ctx context.Context, request apiv1.GetGrafanaTemplateRequestObject) (apiv1.GetGrafanaTemplateResponseObject, error) {
	tpl, err := template.New("connection_string").Parse(DashboardTemplate)
	if err != nil {
		log.Error().Err(err).Msg("failed to parse grafana template")

		return apiv1.GetGrafanaTemplate500JSONResponse{
			Reason: err.Error(),
			Code:   TemplateError,
		}, nil
	}

	var b bytes.Buffer

	data := struct {
		TableName string
	}{
		TableName: fmt.Sprintf("spacecraft_%s", request.Id),
	}
	err = tpl.Execute(&b, data)
	if err != nil {
		log.Error().Err(err).Msg("failed to execute grafana template")

		return apiv1.GetGrafanaTemplate500JSONResponse{
			Reason: err.Error(),
			Code:   TemplateError,
		}, nil
	}

	return apiv1.GetGrafanaTemplate200TextResponse(b.String()), nil
}

func get[T any](data map[string]interface{}, key string) (T, error) {
	var zero T

	i, ok := data[key]
	if !ok {
		return zero, fmt.Errorf("key %s not found", key)
	}

	v, ok := i.(T)
	if !ok {
		return zero, fmt.Errorf("failed to convert %s from %T (value %+#v)", key, i, i)
	}

	return v, nil
}

func mapToMission(data map[string]interface{}) (apiv1.Mission, error) {
	m := apiv1.Mission{}

	id, err := get[int32](data, "id")
	if err != nil {
		return m, err
	}
	m.Id = int(id)

	name, err := get[string](data, "name")
	if err != nil {
		return m, err
	}
	m.Name = name

	fields, err := get[string](data, "fields")
	if err != nil {
		return m, err
	}
	m.Fields = fields

	description, err := get[string](data, "description")
	if err != nil {
		return m, nil
	}
	m.Description = &description

	return m, nil
}

func structToMap(v any) (map[string]interface{}, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}

	var m map[string]interface{}
	if err := json.Unmarshal(b, &m); err != nil {
		return nil, err
	}

	return m, nil
}

func logWarn(errs []error) {
	for i, err := range errs {
		if err != nil {
			log.Warn().Err(err).Msgf("failed to send message (%d)", i)
		}
	}
}
