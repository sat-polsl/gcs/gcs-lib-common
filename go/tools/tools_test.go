/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package tools_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/tools"
)

type ToolsSuite struct {
	suite.Suite
}

func TestToolsSuite(t *testing.T) {
	suite.Run(t, new(ToolsSuite))
}

func BenchmarkReverse(b *testing.B) {
	for i := 0; i < b.N; i++ {
		tools.Reverse("test")
	}
}

func (s *ToolsSuite) TestReverse() {
	matrix := []struct {
		input    string
		expected string
	}{
		{"test", "tset"},
		{"", ""},
		{"a", "a"},
		{"ab", "ba"},
		{"abc", "cba"},
	}

	for _, test := range matrix {
		assert.Equal(s.T(), tools.Reverse(test.input), test.expected)
	}
}

func (s *ToolsSuite) TestSetEnvs() {
	matrix := []struct {
		envs map[string]string
	}{
		{map[string]string{"TEST1": "test1", "TEST2": "test2"}},
		{map[string]string{"TEST1": "test1"}},
		{map[string]string{"TEST2": "test2"}},
		{map[string]string{}},
	}

	for _, env := range matrix {
		assert.NoError(s.T(), tools.SetEnvs(env.envs))
	}
}

func (s *ToolsSuite) TestMoreThanOneOK() {
	matrix := []struct {
		okList   []bool
		expected bool
	}{
		{[]bool{true, true, true}, true},
		{[]bool{true, false, true}, true},
		{[]bool{false, false, false}, false},
		{[]bool{true, false, false}, false},
		{[]bool{}, false},
	}

	for _, ok := range matrix {
		assert.Equal(s.T(), tools.MoreThanOneOK(ok.okList...), ok.expected)
	}
}

func (s *ToolsSuite) TestAllOK() {
	matrix := []struct {
		okList   []bool
		expected bool
	}{
		{[]bool{true, true, true}, true},
		{[]bool{true, false, true}, false},
		{[]bool{false, false, false}, false},
		{[]bool{true, false, false}, false},
		{[]bool{}, false},
	}

	for _, ok := range matrix {
		assert.Equal(s.T(), tools.AllOK(ok.okList...), ok.expected)
	}
}
