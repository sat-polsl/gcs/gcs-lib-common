/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Package tools is a collection of tool functions that are used in the server
// and api packages.
package tools

import (
	"context"
	"os"
	"strings"

	"github.com/rs/zerolog/log"
)

func ErrContains(err error, s string) bool {
	return strings.Contains(err.Error(), s)
}

type ResponseMap map[string]interface{}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func SetEnvs(envs map[string]string) error {
	for k, v := range envs {
		if err := os.Setenv(k, v); err != nil {
			return err
		}
	}

	return nil
}

func AllOK(okList ...bool) bool {
	if len(okList) < 1 {
		return false
	}

	for _, status := range okList {
		if !status {
			return false
		}
	}

	return true
}

func MoreThanOneOK(okList ...bool) bool {
	count := 0

	for _, status := range okList {
		if status {
			count++
		}
	}

	return count > 1
}

func WrapContext(ctx context.Context, f func(ctx context.Context) error) {
	if err := f(ctx); err != nil {
		log.Error().Caller().Err(err).Msg("error occurred in wrapped function")
	}
}

func Wrap(f func() error) {
	if err := f(); err != nil {
		log.Error().Caller().Err(err).Msg("error occurred in wrapped function")
	}
}
