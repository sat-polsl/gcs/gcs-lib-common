/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package fake

import (
	"context"
	"errors"
	"fmt"

	grpcGen "gitlab.com/sat-polsl/gcs/gcs-lib-common/go/grpc"
	"google.golang.org/grpc"
)

const (
	FailGet = "X-Fail/Get"
	FailSet = "X-Fail/Set"
)

type Client struct {
	options []*grpcGen.Option
}

var _ grpcGen.ConfiguratorClient = (*Client)(nil)

func New(objs ...any) *Client {
	client := &Client{}

	for _, obj := range objs {
		switch obj := obj.(type) {
		case *grpcGen.Option:
			client.options = append(client.options, obj)

		default:
			panic(fmt.Sprintf("fake client does not support the provided option %T", obj))
		}
	}

	return client
}

func (c *Client) Get(ctx context.Context, in *grpcGen.GetRequest, opts ...grpc.CallOption) (*grpcGen.GetResponse, error) {
	x := ctx.Value(FailGet)
	if x != nil {
		return nil, x.(error)
	}

	for _, option := range c.options {
		if option.Name == in.Option.Name {
			return &grpcGen.GetResponse{
				Option: option,
			}, nil
		}
	}

	return nil, errors.New("option not found")
}

func (c *Client) Set(ctx context.Context, in *grpcGen.SetRequest, opts ...grpc.CallOption) (*grpcGen.SetResponse, error) {
	x := ctx.Value(FailSet)
	if x != nil {
		return nil, x.(error)
	}

	for _, option := range c.options {
		if option.Name == in.Option.Name {
			return nil, nil
		}
	}

	c.options = append(c.options, in.Option)

	return &grpcGen.SetResponse{
		Option: in.Option,
	}, nil
}
