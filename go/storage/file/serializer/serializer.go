/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package serializer

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"strings"
	"text/template"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/tools"
)

type Serializer struct {
	IndentSize int
}

var defaultSerializer = Serializer{
	IndentSize: 2,
}

func Marshal(data [][]byte) (out []byte, err error) {
	return defaultSerializer.Marshal(data)
}

func (s Serializer) Marshal(data [][]byte) (out []byte, err error) {
	// NOTE: this is not implemented yet
	// if defaultSerializer.MaxDepth(data) == 1 {
	// 	// TODO: implement CSV serializer
	// 	// return defaultSerializer.ToCSV(data)
	// 	return defaultSerializer.ToJSONArray(data)
	// } else {
	// 	return defaultSerializer.ToJSONArray(data)
	// }
	return s.ToJSONArray(data)
}

func (s Serializer) MaxDepth(data [][]byte) int {
	maxDepth := 0

	for _, b := range data {
		depth := 0

		for _, r := range string(b) {
			switch r {
			case '[', '{':
				depth++
			case ']', '}':
				depth--
			}

			if depth > maxDepth {
				maxDepth = depth
			}
		}
	}

	return maxDepth
}

func (s Serializer) ToCSV(data [][]byte) (out []byte, err error) {
	// TODO:: Implement
	// for _, b := range data {
	// 	if err = json.Unmarshal(b,); err != nil {
	// 		err = fmt.Errorf("unable to unmarshall data: %w", err)
	// 		return
	// 	}
	// }

	var writer strings.Builder
	csv.NewWriter(&writer)

	return []byte(writer.String()), nil
}

func (s Serializer) prettifyJSON(data [][]byte) (out [][]byte, err error) {
	j := make(map[string]interface{})

	for _, b := range data {
		if err = json.Unmarshal(b, &j); err != nil {
			err = fmt.Errorf("unable to unmarshall data: %w", err)
			return
		}

		b, err = json.MarshalIndent(j, "", strings.Repeat(" ", s.IndentSize))
		if err != nil {
			err = fmt.Errorf("unable to marshall data with indentation: %w", err)
			return
		}

		out = append(out, b)
	}

	return
}

type wrapper struct {
	Array []string
}

func (w wrapper) Fill(data [][]byte) wrapper {
	var sData []string
	for _, b := range data {
		sData = append(sData, string(b))
	}

	w.Array = sData

	return w
}

func (s Serializer) RemoveLast(data []byte, rm rune) (out []byte) {
	rev := tools.Reverse(string(data))
	for i, r := range rev {
		if r == rm {
			rev = tools.Reverse(string(append([]rune(rev[0:i]), []rune(rev[i+1:])...)))
			return []byte(rev)
		}
	}
	return []byte(tools.Reverse(rev))
}

func (s Serializer) ToJSONArray(data [][]byte) (out []byte, err error) {
	if s.IndentSize > 0 {
		data, err = s.prettifyJSON(data)
		if err != nil {
			err = fmt.Errorf("unable to prettify data: %w", err)
			return
		}
	}

	td := wrapper{}.Fill(data)

	t, err := template.New("x").Parse("[\n{{range .Array}}{{.}},\n{{end}}]")
	if err != nil {
		err = fmt.Errorf("unable to create template: %w", err)
		return
	}

	var tpl bytes.Buffer

	err = t.Execute(&tpl, td)
	if err != nil {
		err = fmt.Errorf("unable to execute template: %w", err)
		return
	}

	return s.RemoveLast(tpl.Bytes(), ','), nil
}
