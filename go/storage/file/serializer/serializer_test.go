/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package serializer_test

import (
	"encoding/json"
	"testing"
	"time"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/file/serializer"
)

var (
	serializerData = map[string]interface{}{
		"spacecraft_id": 1,
		"timestamp":     time.Now().Unix(),
		"latitude":      1.1,
		"longitude":     2.2,
		"altitude":      3.3,
	}

	depth2Data = map[string]interface{}{
		"spacecraft_id": 1,
		"timestamp":     time.Now().Unix(),
		"position": map[string]interface{}{
			"latitude":  1.1,
			"longitude": 2.2,
			"altitude":  3.3,
		},
	}
)

func TestToJSONArray(t *testing.T) {
	s := serializer.Serializer{
		IndentSize: 2,
	}

	b, err := json.Marshal(serializerData)
	if err != nil {
		t.Errorf("unable to marshall data: %s", err.Error())
	}

	_, err = s.ToJSONArray([][]byte{b})
	if err != nil {
		t.Errorf("unable to convert data to JSON array: %s", err.Error())
	}
}

func TestMaxDepth(t *testing.T) {
	s := serializer.Serializer{
		IndentSize: 2,
	}

	b, err := json.Marshal(depth2Data)
	if err != nil {
		t.Errorf("unable to marshall data: %s", err.Error())
	}

	depth := s.MaxDepth([][]byte{b})
	if depth != 2 {
		t.Errorf("depth calculation error: got %d, wanted: %d", depth, 2)
	}
}

func TestToCSV(t *testing.T) {
	s := serializer.Serializer{
		IndentSize: 2,
	}

	b, err := json.Marshal(serializerData)
	if err != nil {
		t.Errorf("unable to marshall data: %s", err.Error())
	}

	_, err = s.ToCSV([][]byte{b})
	if err != nil {
		t.Errorf("unable to convert data to JSON array: %s", err.Error())
	}
}
