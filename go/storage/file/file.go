/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package file

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"path"
	"sync"
	"time"

	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/file/serializer"
)

var (
	tracer = otel.Tracer("gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/file")
)

type Client struct {
	contentDump   bool
	data          map[string][][]byte
	mux           sync.Mutex
	pathPrefix    string
	fileExtension string
	shardSize     int64
}

var _ storage.Storage = (*Client)(nil)

type Option func(*Client)

func NewClient(ctx context.Context, opts ...Option) *Client {
	client := &Client{
		data:          make(map[string][][]byte),
		shardSize:     DefaultShardSize,
		pathPrefix:    DefaultPathPrefix,
		fileExtension: DefaultFileExtension,
		contentDump:   DefaultEmergencyContentDump,
	}

	for _, opt := range opts {
		opt(client)
	}

	return client
}

func WithContentDump(dumpOnError bool) Option {
	return func(c *Client) {
		c.contentDump = dumpOnError
	}
}

func WithShardSize(size int64) Option {
	return func(c *Client) {
		if size > 0 {
			c.shardSize = size
		}
	}
}

func WithStoragePath(p string) Option {
	return func(c *Client) {
		if p != "" {
			c.pathPrefix = p
		}
	}
}

func WithFileType(filetype string) Option {
	return func(c *Client) {
		if filetype != "" {
			c.fileExtension = filetype
		}
	}
}

func WithTables(tables map[string][]map[string]interface{}) Option {
	return func(c *Client) {
		for table, data := range tables {
			for _, d := range data {
				b, err := json.Marshal(d)
				if err != nil {
					log.Error().Err(err).Msg("unable to marshall data into JSON")
					continue
				}

				c.data[table] = append(c.data[table], b)
			}
		}
	}
}

// Get
func (c *Client) Get(ctx context.Context, table string, options storage.GetOptions) ([]map[string]interface{}, error) {
	var (
		limit, offset int
		limitOk       = false
		offsetOk      = false
	)

	if options != nil {
		limit, limitOk = options.Limit()
		offset, offsetOk = options.Offset()
	}

	data, ok := c.data[table]
	if !ok {
		return nil, fmt.Errorf("table '%s' not found", table)
	}

	var out []map[string]interface{}

	for i, b := range data {
		if offsetOk && i < offset {
			continue
		}

		if limitOk && i >= limit {
			break
		}

		var m map[string]interface{}
		if err := json.Unmarshal(b, &m); err != nil {
			return nil, fmt.Errorf("unable to unmarshall data from JSON: %w", err)
		}

		out = append(out, m)
	}

	return out, nil
}

// Insert
func (c *Client) Insert(ctx context.Context, table string, data map[string]interface{}) error {
	_, span := tracer.Start(ctx, "Insert")
	defer span.End()

	c.mux.Lock()
	defer c.mux.Unlock()

	b, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("unable to marshall data into JSON: %w", err)
	}

	c.data[table] = append(c.data[table], b)

	return nil
}

type Spacecraft struct {
	id    string
	store *Client
}

func (c *Client) Spacecraft(id string) storage.Spacecraft {
	return &Spacecraft{
		id:    id,
		store: c,
	}
}

// SaveData saves data to the storage
func (s *Spacecraft) SaveData(ctx context.Context, m map[string]interface{}) error {
	_, span := tracer.Start(ctx, "Save")
	defer span.End()

	s.store.mux.Lock()
	defer s.store.mux.Unlock()

	b, err := json.Marshal(m)
	if err != nil {
		return fmt.Errorf("unable to marshall data into JSON: %w", err)
	}

	id, ok := m["spacecraft_id"]
	if !ok {
		return fmt.Errorf("unable to find 'spacecraft_id' in the message")
	}

	fID, ok := id.(float64)
	if !ok {
		return fmt.Errorf("unable to convert 'spacecraft_id' to float64")
	}

	sid := fmt.Sprintf("spacecraft_%d", int(fID))

	s.store.data[sid] = append(s.store.data[sid], b)

	return nil
}

func (s *Spacecraft) GetData(ctx context.Context, options storage.GetOptions) ([]map[string]interface{}, error) {
	return s.store.Get(ctx, fmt.Sprintf("spacecraft_%s", s.id), options)
}

// Close closes the client
func (c *Client) Close(ctx context.Context) error {
	log.Info().Msg("closing files")
	errs := c.dump()
	if len(errs) != 0 {
		return fmt.Errorf("one or more errors received during dumping the data: %v", errs)
	}

	return nil
}

func (c *Client) Create(ctx context.Context, table string, options storage.CreateOptions) error {
	return ErrNotSupported
}

func (c *Client) Update(ctx context.Context, table string, params map[string]interface{}, options storage.UpdateOptions) error {
	return ErrNotSupported
}

func (c *Client) Delete(ctx context.Context, table string, options storage.DeleteOptions) error {
	return ErrNotSupported
}

func concat(bbs [][]byte) (out []byte) {
	for _, bs := range bbs {
		out = append(out, bs...)
		out = append(out, []byte("\n")...)
	}

	return out
}

func (c *Client) dump() (errs []error) {
	c.mux.Lock()
	defer c.mux.Unlock()

	log.Info().Str("location", c.pathPrefix).Str("file_extension", c.fileExtension).Msg("dumping files")

	for spacecraftID, data := range c.data {
		log.Trace().Caller().Str("spacecraft_id", spacecraftID).Msg("dumping files")

		if stat, err := os.Stat(c.pathPrefix); errors.Is(err, os.ErrNotExist) {
			if err := os.MkdirAll(c.pathPrefix, 0764); err != nil {
				errs = append(errs, err)
				if c.contentDump {
					fmt.Printf("-|%s|%s\n", spacecraftID, string(concat(data)))
				}
				continue
			}
		} else if !stat.IsDir() {
			errs = append(errs, fmt.Errorf("%s is not a directory", c.pathPrefix))
			if c.contentDump {
				fmt.Printf("-|%s|%s\n", spacecraftID, string(concat(data)))
			}
			continue
		}

		log.Trace().Caller().Msg("starting sharding")
		sharded := c.shard(data)
		log.Trace().Caller().Int("shards", len(sharded)).Msg("sharding done")
		for _, s := range sharded {
			fname := path.Join(c.pathPrefix, fmt.Sprintf("%s-%d.%s",
				spacecraftID,
				time.Now().UnixMilli(),
				c.fileExtension))

			b, err := serializer.Marshal(s)
			if err != nil {
				errs = append(errs, err)
				if c.contentDump {
					fmt.Printf("%s|%s|%s\n", fname, spacecraftID, string(concat(s)))
				}
				continue
			}

			log.Trace().Caller().Int("data_size", len(b)).Msg("writing data")
			err = os.WriteFile(fname, b, 0600)
			if err != nil {
				errs = append(errs, err)
				if c.contentDump {
					fmt.Printf("%s|%s|%s\n", fname, spacecraftID, string(concat(s)))
				}
				continue
			}
		}

		c.data[spacecraftID] = [][]byte{}
	}

	return errs
}

func (c *Client) shard(data [][]byte) (out [][][]byte) {
	dataSize := int64(0)
	chunkBeginning := 0
	for i, b := range data {
		dataSize += int64(len(b))
		if dataSize >= c.shardSize {
			log.Trace().Caller().Int64("size", dataSize).Msg("creating chunk")
			dataSize = 0
			out = append(out, data[chunkBeginning:i-1])
			chunkBeginning = i - 1
		}
	}

	if chunkBeginning < len(data) {
		out = append(out, data[chunkBeginning:])
	}

	if len(out) == 0 {
		out = append(out, data)
	}

	return
}
