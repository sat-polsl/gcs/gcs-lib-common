/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package file_test

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path"
	"testing"
	"time"

	"github.com/docker/go-units"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/file"
)

type FileTestSuite struct {
	suite.Suite
	pathPrefix     string
	numberOfShards int
}

func (s *FileTestSuite) SetupSuite() {
	s.pathPrefix = "tmp"
	s.numberOfShards = 4
}

var (
	testFileData = map[string]interface{}{
		"spacecraft_id": 1.0,
		"timestamp":     time.Now().Unix(),
		"latitude":      1.1,
		"longitude":     2.2,
		"altitude":      3.3,
	}
)

func TestFileTestSuite(t *testing.T) {
	suite.Run(t, new(FileTestSuite))
}

func (s *FileTestSuite) TearDownSuite() {
	if !s.T().Failed() {
		_ = os.RemoveAll(s.pathPrefix)
	}
}

func (s *FileTestSuite) TestNewClient() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClient(ctx)

	err := fc.Close(ctx)
	assert.NoError(s.T(), err)
}

func (s *FileTestSuite) TestSave() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClient(ctx,
		file.WithStoragePath(s.pathPrefix),
	)

	err := fc.Spacecraft("1").SaveData(ctx, testFileData)
	require.NoError(s.T(), err)

	id := int(testFileData["spacecraft_id"].(float64))

	data, err := fc.Get(ctx, fmt.Sprintf("spacecraft_%d", id), nil)
	require.NoError(s.T(), err)
	require.NotEmpty(s.T(), data)
	// override timestamp as it is returned in exponential format
	data[0]["timestamp"] = testFileData["timestamp"]
	assert.Equal(s.T(), testFileData, data[0])
}

func (s *FileTestSuite) TestCreate() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClient(ctx,
		file.WithStoragePath(s.pathPrefix),
	)

	err := fc.Create(ctx, "test", nil)
	require.ErrorIs(s.T(), err, file.ErrNotSupported)
}

func (s *FileTestSuite) TestDelete() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClient(ctx,
		file.WithStoragePath(s.pathPrefix),
	)

	err := fc.Delete(ctx, "test", nil)
	require.ErrorIs(s.T(), err, file.ErrNotSupported)
}

func (s *FileTestSuite) TestClose() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClient(ctx,
		file.WithStoragePath(s.pathPrefix),
	)

	for i := 0; i < 10; i++ {
		if err := fc.Spacecraft("1").SaveData(ctx, testFileData); err != nil {
			assert.NoError(s.T(), err)
		}
	}

	err := fc.Close(ctx)
	require.NoError(s.T(), err)
	assert.DirExists(s.T(), s.pathPrefix)
}

func (s *FileTestSuite) TestShard() {
	pathPrefixSharding := path.Join(s.pathPrefix, "sharding")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	fc := file.NewClient(ctx,
		file.WithStoragePath(pathPrefixSharding),
		file.WithShardSize(1*units.MiB),
	)

	marshaled, err := json.Marshal(testFileData)
	require.NoError(s.T(), err)

	for i := 0; i < (s.numberOfShards*units.MiB)/len(marshaled); i++ {
		if err = fc.Spacecraft("1").SaveData(ctx, testFileData); err != nil {
			assert.NoError(s.T(), err)
		}
	}

	err = fc.Close(ctx)
	require.NoError(s.T(), err)

	require.DirExists(s.T(), pathPrefixSharding)

	f, err := os.ReadDir(pathPrefixSharding)
	require.NoError(s.T(), err)
	assert.Equal(s.T(), s.numberOfShards, len(f))
}
