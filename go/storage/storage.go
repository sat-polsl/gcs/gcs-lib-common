/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

// Package storage is implementing structure and all necessary substructures and
// interfaces that are intended to run as a thread saving (and possibly reading)
// data to/from the storage.
//
// Storage itself is abstracted behind Backend interface - it can be MongoDB,
// Text file (JSON or CSV?) and possibly SQLite.
package storage

import (
	"context"
)

type Storage interface {
	Spacecraft(string) Spacecraft
	Get(context.Context, string, GetOptions) ([]map[string]interface{}, error)
	Insert(context.Context, string, map[string]interface{}) error
	Create(context.Context, string, CreateOptions) error
	Delete(context.Context, string, DeleteOptions) error
	Update(context.Context, string, map[string]interface{}, UpdateOptions) error
	Close(context.Context) error
}

type Spacecraft interface {
	SaveData(context.Context, map[string]interface{}) error
	GetData(context.Context, GetOptions) ([]map[string]interface{}, error)
}

type GetOptions interface {
	Limit() (int, bool)
	Offset() (int, bool)
	Columns() ([]string, bool)
	Filters() ([]Filter, bool)
}

type Filter interface {
	Field() string
	Operator() string
	Value() interface{}
}

type CreateOptions interface {
	Fields() []Field
}

type Field struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type DeleteOptions interface {
	Filters() ([]Filter, bool)
}

type UpdateOptions interface {
	Filters() ([]Filter, bool)
}
