/* Copyright © 2023 Silesian Aerospace Technologies, GCS Authors
Full license text is available in the LICENSE file */
CREATE TABLE IF NOT EXISTS missions (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT,
    fields TEXT NOT NULL
);
