/* Copyright © 2023 Silesian Aerospace Technologies, GCS Authors
Full license text is available in the LICENSE file */
CREATE TABLE IF NOT EXISTS spacecraft_1 (
    _id SERIAL,
    timestamp DOUBLE PRECISION,
    latitude DOUBLE PRECISION,
    longitude DOUBLE PRECISION,
    altitude DOUBLE PRECISION,
    spacecraft_id INT
);

CREATE TABLE IF NOT EXISTS test_update (
    _id SERIAL,
    timestamp DOUBLE PRECISION,
    latitude DOUBLE PRECISION,
    longitude DOUBLE PRECISION,
    altitude DOUBLE PRECISION,
    spacecraft_id INT
);

INSER INTO test_update (timestamp, latitude, longitude, altitude, spacecraft_id) VALUES (NOW(), 1, 2, 3, 4);

CREATE TABLE IF NOT EXISTS test_delete (
    _id SERIAL,
    timestamp DOUBLE PRECISION,
    latitude DOUBLE PRECISION,
    longitude DOUBLE PRECISION,
    altitude DOUBLE PRECISION,
    spacecraft_id INT
);

INSER INTO test_delete (timestamp, latitude, longitude, altitude, spacecraft_id) VALUES (NOW(), 1, 2, 3, 4);
