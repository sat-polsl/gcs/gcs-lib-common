/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package postgres

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"text/template"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jeremywohl/flatten"
	"github.com/rs/zerolog/log"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
	"go.opentelemetry.io/otel"
)

var (
	tracer = otel.Tracer("gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/postgres")
)

type ConnectionPool interface {
	Begin(context.Context) (pgx.Tx, error)
	Close()
}

type Client struct {
	pgPool ConnectionPool
	schema string
	config config
}

var _ storage.Storage = (*Client)(nil)

type Option func(*Client)

type config struct {
	Host     string
	Port     string
	Database string
	Username string
	Password string
}

func WithCredentials(username, password string) Option {
	return func(c *Client) {
		if username != "" {
			c.config.Username = username
		}
		if password != "" {
			c.config.Password = password
		}
	}
}

func WithDatabase(database string) Option {
	return func(c *Client) {
		if database != "" {
			c.config.Database = database
		}
	}
}

func WithHost(host string) Option {
	return func(c *Client) {
		if host != "" {
			c.config.Host = host
		}
	}
}

func WithPort(port string) Option {
	return func(c *Client) {
		if port != "" {
			c.config.Port = port
		}
	}
}

func WithConnectionPool(cp ConnectionPool) Option {
	return func(c *Client) {
		c.pgPool = cp
	}
}

func WithSchema(schema string) Option {
	return func(c *Client) {
		c.schema = schema
	}
}

func New(ctx context.Context, options ...Option) (*Client, error) {
	client := &Client{
		config: config{
			Host:     DefaultPostgresHost,
			Port:     DefaultPostgresPort,
			Database: DefaultPostgresDB,
		},
	}

	connTpl, err := template.New("connection_string").Parse(ConnectionString)
	if err != nil {
		return nil, fmt.Errorf("unable to create template from connection string: %w", err)
	}

	for _, opt := range options {
		opt(client)
	}

	var bytesTpl bytes.Buffer
	err = connTpl.Execute(&bytesTpl, client.config)
	if err != nil {
		return nil, fmt.Errorf("unable to execute template: %w", err)
	}
	log.Trace().Caller().Str("connection_string", bytesTpl.String()).Msg("connection string created")

	pgPool, err := pgxpool.Connect(ctx, bytesTpl.String())
	if err != nil {
		return nil, fmt.Errorf("unable to connect to PostgreSQL (%s): %w", bytesTpl.String(), err)
	}
	client.pgPool = pgPool

	if client.schema == "" {
		log.Warn().Caller().Msg("schema not provided, using default schema")
		client.schema = defaultSchema
	}

	err = client.createSchema(ctx, client.schema)
	if err != nil {
		return nil, fmt.Errorf("unable to create schema: %w", err)
	}

	return client, nil
}

func (c *Client) Spacecraft(id string) storage.Spacecraft {
	return &Spacecraft{
		id:    id,
		store: c,
	}
}

// Close closes the postgres client.
func (c *Client) Close(ctx context.Context) error {
	c.pgPool.Close()
	return nil
}

// Get returns rows from the table.
// If options are provided, they are used to filter the results.
func (c *Client) Get(ctx context.Context, table string, options storage.GetOptions) ([]map[string]interface{}, error) {
	ctx, span := tracer.Start(ctx, "Get")
	defer span.End()

	query, args := c.buildSelect(table, options)
	tx, err := c.pgPool.Begin(ctx)
	if err != nil {
		return nil, fmt.Errorf("unable to begin transaction: %w", err)
	}
	//nolint: errcheck
	defer tx.Rollback(ctx)

	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("unable to query: %w", err)
	}

	var result []map[string]interface{}
	for rows.Next() {
		val, err := rows.Values()
		if err != nil {
			return nil, fmt.Errorf("unable to get values: %w", err)
		}

		m := make(map[string]interface{})
		for i, col := range rows.FieldDescriptions() {
			m[string(col.Name)] = val[i]
		}

		result = append(result, m)
	}

	return result, nil
}

// Insert inserts the provided data into the table.
func (c *Client) Insert(ctx context.Context, table string, data map[string]interface{}) error {
	ctx, span := tracer.Start(ctx, "Insert")
	defer span.End()

	query, args, err := buildInsert(table, data)
	if err != nil {
		return fmt.Errorf("unable to build INSERT query: %w", err)
	}

	tx, err := c.pgPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to begin transaction: %w", err)
	}
	//nolint: errcheck
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return fmt.Errorf("unable to insert data: %w", err)
	}

	return tx.Commit(ctx)
}

// Create creates a table with the provided options.
func (c *Client) Create(ctx context.Context, table string, options storage.CreateOptions) error {
	ctx, span := tracer.Start(ctx, "Create")
	defer span.End()

	query, err := c.buildCreate(table, options)
	if err != nil {
		return fmt.Errorf("unable to build CREATE query: %w", err)
	}

	tx, err := c.pgPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to begin transaction: %w", err)
	}
	//nolint: errcheck
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, query)
	if err != nil {
		return fmt.Errorf("unable to create table: %w", err)
	}

	return tx.Commit(ctx)
}

// Update updates the table with the provided params.
func (c *Client) Update(ctx context.Context, table string, params map[string]interface{}, options storage.UpdateOptions) error {
	ctx, span := tracer.Start(ctx, "Update")
	defer span.End()

	query, args, err := c.buildUpdate(table, params, options)
	if err != nil {
		return fmt.Errorf("unable to build CREATE query: %w", err)
	}

	tx, err := c.pgPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to begin transaction: %w", err)
	}
	//nolint: errcheck
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return fmt.Errorf("unable to create table: %w", err)
	}

	return tx.Commit(ctx)
}

// Delete deletes the rows from the table.
func (c *Client) Delete(ctx context.Context, table string, options storage.DeleteOptions) error {
	ctx, span := tracer.Start(ctx, "Delete")
	defer span.End()

	query, args, err := c.buildDelete(table, options)
	if err != nil {
		return fmt.Errorf("unable to build CREATE query: %w", err)
	}

	tx, err := c.pgPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to begin transaction: %w", err)
	}
	//nolint: errcheck
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, query, args...)
	if err != nil {
		return fmt.Errorf("unable to create table: %w", err)
	}

	return tx.Commit(ctx)
}

func buildInsert(table string, data map[string]interface{}) (query string, values []interface{}, err error) {
	tplStr := `INSERT INTO %s(%s) VALUES (%s);`

	flatData, err := flatten.Flatten(data, "", flatten.UnderscoreStyle)
	if err != nil {
		return "", nil, fmt.Errorf("unable to flatten document: %w", err)
	}

	columns, values := divide(flatData)

	query = fmt.Sprintf(tplStr, table, strings.Join(columns, ", "), enumerate(values))

	return query, values, nil
}

func divide(flatData map[string]interface{}) (columns []string, values []interface{}) {
	for k, v := range flatData {
		columns = append(columns, k)
		values = append(values, v)
	}

	return
}

func enumerate(d []interface{}) string {
	str := ""
	for i := range d {
		if i == 0 {
			str = fmt.Sprintf("$%d", i+1)
		} else {
			str += fmt.Sprintf(", $%d", i+1)
		}
	}

	return str
}

func (c *Client) buildSelect(table string,
	options storage.GetOptions) (query string, args []interface{}) {
	tplStr := `SELECT %s FROM %s`

	var (
		limit, offset int
		cols          []string
		ok            bool
	)

	if options != nil {
		cols, ok = options.Columns()
		if !ok {
			cols = []string{"*"}
		}

		limit, ok = options.Limit()
		if ok {
			tplStr += fmt.Sprintf(" LIMIT %d", limit)
		} else {
			tplStr += " LIMIT 100"
		}

		offset, ok = options.Offset()
		if ok {
			tplStr += fmt.Sprintf(" OFFSET %d", offset)
		}

		filters, ok := options.Filters()
		if ok {
			tplStr += " WHERE "
			for _, filter := range filters {
				tplStr += fmt.Sprintf("%s %s %v", filter.Field(), filter.Operator(), len(args)+1)
				args = append(args, filter.Value())
			}
		}
	} else {
		cols = []string{"*"}
		tplStr += " LIMIT 100"
	}

	return fmt.Sprintf(tplStr+";", strings.Join(cols, ", "), table), args
}

func (c *Client) buildCreate(table string, options storage.CreateOptions) (string, error) {
	tplStr := `CREATE TABLE %s ( %s );`

	columns := []string{}

	if options == nil {
		return "", ErrNilOptions
	}

	for _, col := range options.Fields() {
		columns = append(columns, fmt.Sprintf("%s %s", col.Name, col.Type))
	}

	return fmt.Sprintf(tplStr, table, strings.Join(columns, ", ")), nil
}

func (c *Client) buildUpdate(table string,
	params map[string]interface{},
	options storage.UpdateOptions) (query string, args []interface{}, err error) {
	tplStr := `UPDATE %s SET %s WHERE %s;`

	columns := []string{}

	if options == nil {
		return "", nil, ErrNilOptions
	}

	for k, v := range params {
		columns = append(columns, fmt.Sprintf("%s = $%d", k, len(args)+1))
		args = append(args, v)
	}

	filters, ok := options.Filters()
	if !ok {
		return "", nil, ErrNilFilters
	}

	for _, filter := range filters {
		columns = append(columns, fmt.Sprintf("%s %s %v", filter.Field(), filter.Operator(), len(args)+1))
		args = append(args, filter.Value())
	}

	return fmt.Sprintf(tplStr, table, strings.Join(columns, ", "), strings.Join(columns, ", ")), args, nil
}

func (c *Client) buildDelete(table string,
	options storage.DeleteOptions) (query string, args []interface{}, err error) {
	tplStr := `DELETE FROM %s WHERE %s;`

	columns := []string{}

	if options == nil {
		return "", nil, ErrNilOptions
	}

	filters, ok := options.Filters()
	if !ok {
		return "", nil, ErrNilFilters
	}

	for _, filter := range filters {
		columns = append(columns, fmt.Sprintf("%s %s %v", filter.Field(), filter.Operator(), len(args)+1))
		args = append(args, filter.Value())
	}

	return fmt.Sprintf(tplStr, table, strings.Join(columns, ", ")), args, nil
}

func (c *Client) createSchema(ctx context.Context, schema string) error {
	ctx, span := tracer.Start(ctx, "createSchema")
	defer span.End()

	tx, err := c.pgPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to begin transaction: %w", err)
	}
	//nolint: errcheck
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, schema)
	if err != nil {
		return fmt.Errorf("unable to create table: %w", err)
	}

	return tx.Commit(ctx)
}
