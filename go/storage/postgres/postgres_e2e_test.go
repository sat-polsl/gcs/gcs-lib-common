//go:build e2e

/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package postgres_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	_ "embed"

	"github.com/jackc/pgx/v4"
	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage/postgres"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/tools"
)

const (
	TestUsername       = "test_user"
	TestPassword       = "test_password"
	TestDB             = "sat_test"
	ConnectionString   = "postgresql://localhost:%s/%s?user=%s&password=%s"
	ConnectionTemplate = "postgresql://{{.Host}}:{{.Port}}/{{.Database}}?user={{.Username}}&password={{.Password}}"
)

var (
	//go:embed templates/test_schema.sql
	SQLTable string

	e2eTestData = map[string]interface{}{
		"spacecraft_id": 1.0,
		"timestamp":     time.Now().Unix(),
		"latitude":      1.1,
		"longitude":     2.2,
		"altitude":      3.3,
	}
)

type PostgresE2ETestSuite struct {
	suite.Suite
	pool         *dockertest.Pool
	postgres     *dockertest.Resource
	postgresHost string
	postgresPort string
}

func TestPostgresE2ETestSuite(t *testing.T) {
	suite.Run(t, new(PostgresE2ETestSuite))
}

func (s *PostgresE2ETestSuite) startPostgres() (*dockertest.Resource, error) {
	resource, err := s.pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "latest",
		Env: []string{
			tools.KeyValue{Key: "POSTGRES_USER", Value: TestUsername}.String(),
			tools.KeyValue{Key: "POSTGRES_PASSWORD", Value: TestPassword}.String(),
			tools.KeyValue{Key: "POSTGRES_DB", Value: TestDB}.String(),
		},
	}, func(config *docker.HostConfig) {
		// set AutoRemove to true so that stopped container goes away by itself
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{
			Name: "no",
		}
	})
	if err != nil {
		return resource, err
	}

	// exponential backoff-retry, because the application in the container might not be ready to accept connections yet
	err = s.pool.Retry(func() error {
		var err error
		s.postgresPort = resource.GetPort("5432/tcp")
		s.postgresHost = "localhost"
		if err != nil {
			return fmt.Errorf("unable to get postgres host and port: %w", err)
		}

		connStr := fmt.Sprintf(ConnectionString, s.postgresPort, TestDB, TestUsername, TestPassword)

		dbClient, err := pgx.Connect(context.TODO(), connStr)
		if err != nil {
			return fmt.Errorf("unable to connect to database (%s): %w", connStr, err)
		}
		defer dbClient.Close(context.TODO())

		if err := dbClient.Ping(context.TODO()); err != nil {
			return fmt.Errorf("unable to ping: %w", err)
		}

		tx, err := dbClient.Begin(context.TODO())
		if err != nil {
			return fmt.Errorf("unable to begin transaction: %w", err)
		}
		defer tx.Rollback(context.TODO())

		if _, err := tx.Exec(context.TODO(), SQLTable); err != nil {
			return fmt.Errorf("unable to execute statement: %w", err)
		}

		if err = tx.Commit(context.TODO()); err != nil {
			return fmt.Errorf("unable to commit transaction: %w", err)
		}

		return nil
	})
	return resource, err
}

func (s *PostgresE2ETestSuite) SetupSuite() {
	var err error

	s.pool, err = dockertest.NewPool("")
	require.NoError(s.T(), err)

	resource, err := s.startPostgres()
	require.NoError(s.T(), err)
	if resource != nil {
		s.postgres = resource
	}
}

func (s *PostgresE2ETestSuite) TearDownSuite() {
	if s.postgres != nil {
		if err := s.pool.Purge(s.postgres); err != nil {
			assert.NoError(s.T(), err)
		}
	}
}

func (s *PostgresE2ETestSuite) TestSaveData() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c, err := postgres.New(ctx,
		postgres.WithHost(s.postgresHost),
		postgres.WithPort(s.postgresPort),
		postgres.WithCredentials(TestUsername, TestPassword),
		postgres.WithDatabase(TestDB),
	)
	require.NoError(s.T(), err)

	id := fmt.Sprintf("%d", int(e2eTestData["id"].(float64)))
	err = c.Spacecraft(id).SaveData(ctx, e2eTestData)
	assert.NoError(s.T(), err)

	err = c.Close(ctx)
	assert.NoError(s.T(), err)
}

func (s *PostgresE2ETestSuite) TestGet() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c, err := postgres.New(ctx,
		postgres.WithHost(s.postgresHost),
		postgres.WithPort(s.postgresPort),
		postgres.WithCredentials(TestUsername, TestPassword),
		postgres.WithDatabase(TestDB),
	)
	require.NoError(s.T(), err)

	_, err = c.Get(ctx, "spacecraft_1", nil)
	assert.NoError(s.T(), err)
}

func (s *PostgresE2ETestSuite) TestCreate() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c, err := postgres.New(ctx,
		postgres.WithHost(s.postgresHost),
		postgres.WithPort(s.postgresPort),
		postgres.WithCredentials(TestUsername, TestPassword),
		postgres.WithDatabase(TestDB),
	)
	require.NoError(s.T(), err)

	opts := &createOpts{}

	err = c.Create(ctx, "test_2", opts)
	assert.NoError(s.T(), err)
}

func (s *PostgresE2ETestSuite) TestUpdate() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c, err := postgres.New(ctx,
		postgres.WithHost(s.postgresHost),
		postgres.WithPort(s.postgresPort),
		postgres.WithCredentials(TestUsername, TestPassword),
		postgres.WithDatabase(TestDB),
	)
	require.NoError(s.T(), err)

	opts := &updateOpts{}
	params := map[string]interface{}{
		"latitude": 1.3,
	}
	err = c.Update(ctx, "test_2", params, opts)
	assert.NoError(s.T(), err)
}

func (s *PostgresE2ETestSuite) TestDelete() {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c, err := postgres.New(ctx,
		postgres.WithHost(s.postgresHost),
		postgres.WithPort(s.postgresPort),
		postgres.WithCredentials(TestUsername, TestPassword),
		postgres.WithDatabase(TestDB),
	)
	require.NoError(s.T(), err)

	opts := &deleteOpts{}

	err = c.Delete(ctx, "test_delete", opts)
	assert.NoError(s.T(), err)
}

type filter struct {
	field    string
	operator string
	value    interface{}
}

func (f *filter) Field() string      { return f.field }
func (f *filter) Operator() string   { return f.operator }
func (f *filter) Value() interface{} { return f.value }

type createOpts struct{}

func (c *createOpts) Fields() []storage.Field {
	return []storage.Field{
		{Name: "name", Type: "string"},
		{Name: "age", Type: "int"},
	}
}

type updateOpts struct{}

func (u *updateOpts) Filters() ([]storage.Filter, bool) {
	return []storage.Filter{
		&filter{field: "latitude", operator: "=", value: 1.0},
	}, false
}

type deleteOpts struct{}

func (u *deleteOpts) Filters() ([]storage.Filter, bool) {
	return []storage.Filter{
		&filter{field: "latitude", operator: "=", value: 1.0},
	}, false
}
