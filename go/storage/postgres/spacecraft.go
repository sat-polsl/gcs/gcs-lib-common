/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package postgres

import (
	"context"
	"fmt"
	"strings"

	"github.com/jeremywohl/flatten"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
)

type Spacecraft struct {
	id    string
	store *Client
}

// Save saves data to the spacecratf table.
func (s *Spacecraft) SaveData(ctx context.Context, data map[string]interface{}) error {
	ctx, span := tracer.Start(ctx, "Save")
	defer span.End()

	insert, values, err := buildSpacecraftInsert(data)
	if err != nil {
		return fmt.Errorf("unable to build insert: %w", err)
	}

	tx, err := s.store.pgPool.Begin(ctx)
	if err != nil {
		return fmt.Errorf("unable to insert: %w", err)
	}
	//nolint: errcheck
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, insert, values...)
	if err != nil {
		return fmt.Errorf("unable to insert: %w", err)
	}

	return tx.Commit(ctx)
}

// Save saves data to the spacecratf table.
func (s *Spacecraft) GetData(ctx context.Context, options storage.GetOptions) ([]map[string]interface{}, error) {
	return s.store.Get(ctx, fmt.Sprintf("spacecraft_%s", s.id), options)
}

func buildSpacecraftInsert(data map[string]interface{}) (query string, values []interface{}, err error) {
	tplStr := `INSERT INTO spacecraft_%d(%s) VALUES (%s);`

	id, ok := data["spacecraft_id"]
	if !ok {
		return "", nil, fmt.Errorf("unable to find 'spacecraft_id' in the data")
	}

	fID, ok := id.(float64)
	if !ok {
		return "", nil, fmt.Errorf("unable to convert 'spacecraft_id' to float64")
	}

	flatData, err := flatten.Flatten(data, "", flatten.UnderscoreStyle)
	if err != nil {
		return "", nil, fmt.Errorf("unable to flatten document: %w", err)
	}

	columns, values := divide(flatData)

	query = fmt.Sprintf(tplStr, int(fID), strings.Join(columns, ", "), enumerate(values))

	return query, values, nil
}
