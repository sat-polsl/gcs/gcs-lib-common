/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package fake

import (
	"context"

	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/storage"
)

type Client struct{}

var _ storage.Storage = (*Client)(nil)

type FailKey string

const (
	FailCreate FailKey = "X-FAIL/create"
	FailInsert FailKey = "X-FAIL/insert"
	FailGet    FailKey = "X-FAIL/get"
	FailUpdate FailKey = "X-FAIL/update"
	FailDelete FailKey = "X-FAIL/delete"
	FailClose  FailKey = "X-FAIL/close"

	FailSaveData FailKey = "X-FAIL/saveData"
	FailGetData  FailKey = "X-FAIL/getData"
)

func (f FailKey) Error() string {
	return string(f)
}

func (c *Client) Spacecraft(id string) storage.Spacecraft {
	return &Spacecraft{}
}

func (c *Client) Get(ctx context.Context, s string, opt storage.GetOptions) ([]map[string]interface{}, error) {
	x := ctx.Value(FailGet)
	if x != nil {
		return nil, x.(error)
	}

	return nil, nil
}

func (c *Client) Insert(ctx context.Context, s string, m map[string]interface{}) error {
	x := ctx.Value(FailInsert)
	if x != nil {
		return x.(error)
	}

	return nil
}

func (c *Client) Create(ctx context.Context, s string, opt storage.CreateOptions) error {
	x := ctx.Value(FailCreate)
	if x != nil {
		return x.(error)
	}

	return nil
}

func (c *Client) Delete(ctx context.Context, s string, opt storage.DeleteOptions) error {
	x := ctx.Value(FailDelete)
	if x != nil {
		return x.(error)
	}

	return nil
}

func (c *Client) Update(ctx context.Context, s string, m map[string]interface{}, opt storage.UpdateOptions) error {
	x := ctx.Value(FailUpdate)
	if x != nil {
		return x.(error)
	}

	return nil
}

func (c *Client) Close(ctx context.Context) error {
	x := ctx.Value(FailClose)
	if x != nil {
		return x.(error)
	}

	return nil
}

type Spacecraft struct{}

func (s *Spacecraft) SaveData(ctx context.Context, m map[string]interface{}) error {
	x := ctx.Value(FailSaveData)
	if x != nil {
		return x.(error)
	}

	return nil
}

func (s *Spacecraft) GetData(ctx context.Context, opt storage.GetOptions) ([]map[string]interface{}, error) {
	x := ctx.Value(FailGetData)
	if x != nil {
		return nil, x.(error)
	}

	return nil, nil
}
