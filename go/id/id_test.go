/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package id_test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/sat-polsl/gcs/gcs-lib-common/go/id"
)

type IDTestSuite struct {
	suite.Suite
}

func TestIDTestSuite(t *testing.T) {
	suite.Run(t, new(IDTestSuite))
}

func (s *IDTestSuite) SetupSuite() {
	err := id.New("testsalt", "abcdefghijklmnopqrstuvwxyz", 10)
	assert.NoError(s.T(), err)
}

func (s *IDTestSuite) TestEncode() {
	for _, test := range []struct {
		id       int
		expected string
		err      bool
	}{
		{0, "odmwenagrq", false},
		{1, "lzrneykeoa", false},
		{math.MaxInt, "vvvwvwrvbpqdzkbbr", false},
		{math.MinInt, "vvvwvwrvbpqdzkbbr", true},
		{-1, "vvvwvwrvbpqdzkbbr", true},
	} {
		encoded, err := id.Encode(test.id)
		if test.err {
			assert.Error(s.T(), err)
		} else {
			assert.NoError(s.T(), err)
			assert.Equal(s.T(), test.expected, encoded)
		}
	}
}

func (s *IDTestSuite) TestDecode() {
	for _, test := range []struct {
		id       string
		expected []int
		err      bool
	}{
		{"odmwenagrq", []int{0}, false},
		{"lzrneykeoa", []int{1}, false},
		{"vvvwvwrvbpqdzkbbr", []int{math.MaxInt}, false},
	} {
		decoded, err := id.Decode(test.id)
		if test.err {
			assert.Error(s.T(), err)
		} else {
			assert.NoError(s.T(), err)
			assert.Equal(s.T(), test.expected, decoded)
		}
	}
}
