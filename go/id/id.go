/*
Copyright © 2023 Silesian Aerospace Technologies, GCS Authors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

package id

import (
	"github.com/rs/zerolog/log"
	hashid "github.com/speps/go-hashids/v2"
)

var (
	// hashID is a hashid Encoder/Decoder instance
	hashID *hashid.HashID
)

func init() {
	var err error
	hashID, err = hashid.New()
	if err != nil {
		log.Error().Err(err).Msg("failed to create hashid instance")
	}
}

func New(salt, alphabet string, minLength int) error {
	// Create a new hashid instance
	hd := hashid.NewData()
	hd.Alphabet = alphabet
	hd.Salt = salt
	hd.MinLength = minLength

	var err error
	hashID, err = hashid.NewWithData(hd)
	return err
}

// Encode an id to a string (hashid). This method returns a single string, and encodes an single int.
func Encode(id int) (string, error) {
	// Encode an id
	return hashID.Encode([]int{id})
}

// Decode an id from a string (hashid). This method returns an array of ints, not a single int.
func Decode(id string) ([]int, error) {
	ids, err := hashID.DecodeWithError(id)
	if err != nil {
		return []int{}, err
	}
	return ids, nil
}
