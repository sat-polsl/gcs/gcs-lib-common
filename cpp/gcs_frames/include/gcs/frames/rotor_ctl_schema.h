#pragma once

#include <nlohmann/json.hpp>

#include <optional>
#include <stdexcept>
#include <optional>
#include "gcs/frames/nlohmann.h"

namespace gcs::frames {

/**
 * @ingroup gcs_frames
 * @{
 */

/**
 * @brief Rotor Control frame
 */
class rotor_ctl {
public:
    /**
     * @brief Default constructor.
     */
    rotor_ctl() = default;

    /**
     * @brief Constructor.
     * @param azimuth azimuth
     * @param elevation elevation
     */
    rotor_ctl(double azimuth, double elevation) : azimuth_{azimuth}, elevation_{elevation} {}

    /**
     * @brief Gets azimuth
     * @return azimuth
     */
    std::optional<double> get_azimuth() const { return azimuth_; }

    /**
     * @brief Sets azimuth
     * @param value azimuth
     */
    void set_azimuth(const double& value) { azimuth_ = value; }

    /**
     * @brief Checks if azimuth has value
     * @return true if azimuth has value
     */
    bool has_azimuth() const { return azimuth_.has_value(); }

    /**
     * @brief Checks if elevation has value
     * @return true if elevation has value
     */
    bool has_elevation() const { return elevation_.has_value(); }

    /**
     * @brief Gets elevation
     * @return Sets elevation
     */
    std::optional<double> get_elevation() const { return elevation_; }

    /**
     * @brief Sets elevation
     * @param value elevation
     */
    void set_elevation(const double& value) { elevation_ = value; }

private:
    std::optional<double> azimuth_{};
    std::optional<double> elevation_{};
};

/** @} */

} // namespace gcs::frames

namespace nlohmann {

inline void from_json(const json& j, gcs::frames::rotor_ctl& x) {
    if (j.contains("azimuth")) {
        x.set_azimuth(j.at("azimuth").get<double>());
    }
    if (j.contains("elevation")) {
        x.set_elevation(j.at("elevation").get<double>());
    }
}

inline void to_json(json& j, const gcs::frames::rotor_ctl& x) {
    j = json::object();
    if(x.has_azimuth()) {
        j["azimuth"] = x.get_azimuth().value();
    }
    if(x.has_elevation()) {
        j["elevation"] = x.get_elevation().value();
    }
}

} // namespace nlohmann
