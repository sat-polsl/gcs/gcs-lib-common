#pragma once

#include <nlohmann/json.hpp>

#include <optional>
#include <stdexcept>
#include "gcs/frames/nlohmann.h"

namespace gcs::frames {
/**
 * @ingroup gcs_frames
 * @{
 */

/**
 * @brief Relays State frame
 */
class relays_state {
public:
    /**
     * @brief Default constructor.
     */
    relays_state() = default;

    /**
     * @brief Constructor.
     * @param filter2_m filter 2m state
     * @param filter70_cm filter 70cm state
     * @param rotator rotator state
     */
    relays_state(bool filter2_m, bool filter70_cm, bool rotator) :
        filter2_m_{filter2_m}, filter70_cm_{filter70_cm}, rotator_{rotator} {}

    /**
     * @brief Gets filter 2m state
     * @return filter 2m state
     */
    bool get_filter2_m() const { return filter2_m_; }

    /**
     * @brief Sets filter 2m state
     * @param value filter 2m state
     */
    void set_filter2_m(const bool& value) { filter2_m_ = value; }

    /**
     * @brief Gets filter 70cm state
     * @return filter 70cm state
     */
    bool get_filter70_cm() const { return filter70_cm_; }

    /**
     * @brief Sets filter 70cm state
     * @param value filter 70cm state
     */
    void set_filter70_cm(const bool& value) { filter70_cm_ = value; }

    /**
     * @brief Gets rotator state
     * @return rotator state
     */
    bool get_rotator() const { return rotator_; }

    /**
     * @brief Sets rotator state
     * @param value rotator state
     */
    void set_rotator(const bool& value) { rotator_ = value; }

private:
    bool filter2_m_;
    bool filter70_cm_;
    bool rotator_;
};

/** @} */
} // namespace gcs::frames

namespace nlohmann {
void from_json(const json& j, gcs_frames::relays_state& x);
void to_json(json& j, const gcs_frames::relays_state& x);

inline void from_json(const json& j, gcs_frames::relays_state& x) {
    x.set_filter2_m(j.at("filter2m").get<bool>());
    x.set_filter70_cm(j.at("filter70cm").get<bool>());
    x.set_rotator(j.at("rotator").get<bool>());
}

inline void to_json(json& j, const gcs_frames::relays_state& x) {
    j = json::object();
    j["filter2m"] = x.get_filter2_m();
    j["filter70cm"] = x.get_filter70_cm();
    j["rotator"] = x.get_rotator();
}
} // namespace nlohmann
