#pragma once
#include <nlohmann/json.hpp>

#ifndef NLOHMANN_OPT_HELPER
#define NLOHMANN_OPT_HELPER
#include <memory>

namespace nlohmann {
template <typename T>
struct adl_serializer<std::optional<T>> {
static void to_json(json & j, const std::optional<T> & opt) {
    if (!opt) j = nullptr; else j = *opt;
}

static std::optional<T> from_json(const json & j) {
    if (j.is_null()) return std::unique_ptr<T>(); else return std::unique_ptr<T>(new T(j.get<T>()));
    }
};
}
#endif

namespace gcs::frames {

inline nlohmann::json get_untyped(const nlohmann::json& j, const char* property) {
    if (j.find(property) != j.end()) {
        return j.at(property).get<nlohmann::json>();
    }
    return nlohmann::json();
}

inline nlohmann::json get_untyped(const nlohmann::json& j, std::string property) {
    return get_untyped(j, property.data());
}

template <typename T>
inline std::optional<T> get_optional(const nlohmann::json & j, const char * property) {
    if (j.find(property) != j.end()) {
        return j.at(property).get<T>();
    }
    return std::nullopt;
}

template <typename T>
inline std::optional<T> get_optional(const nlohmann::json & j, std::string property) {
    return get_optional<T>(j, property.data());
}

} // namespace gcs::frames
