#pragma once

#include <nlohmann/json.hpp>

#include <optional>
#include <stdexcept>
#include "gcs/frames/nlohmann.h"

namespace gcs::frames {
/**
 * @ingroup gcs_frames
 * @{
 */

/**
 * @brief Relays State frame
 */
class rotator_status {
public:

    rotator_status() = default;
    /**
     * @brief Constructor.
     * @param status status
     */
    rotator_status(std::uint32_t status): status_{status} {}

    /**
     * @brief Gets status
     * @return status
     */
    std::uint32_t get_status() const {return status_;}

    /**
     * @brief Sets status
     * @param value status
     */
    void set_status(const std::uint32_t& value) {status_ = value;}

private:
    std::uint32_t status_;
};

}// namespace gcs::frames

namespace nlohmann {

inline void from_json(const json& j, gcs::frames::rotator_status& x) {
    x.set_status(j.at("status").get<std::uint32_t>());

}

inline void to_json(json& j, const gcs::frames::rotator_status& x) {
    j = json::object();
    j["status"] = x.get_status();
}

} // namespace nlohmann