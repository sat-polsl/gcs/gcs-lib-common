#pragma once

#include <nlohmann/json.hpp>

#include <optional>
#include <stdexcept>
#include "gcs/frames/nlohmann.h"

namespace gcs::frames {
/**
 * @ingroup gcs_frames
 * @{
 */

/**
 * @brief Set Relays frame
 */
class set_relays {
public:
    /**
     * @brief Default constructor.
     */
    set_relays() = default;

    /**
     * @brief Constructor.
     * @param filter2_m optional filter 2m state
     * @param filter70_cm optional filter 70cm state
     * @param rotator optional rotator state
     */
    set_relays(std::optional<bool> filter2_m, std::optional<bool> filter70_cm,
               std::optional<bool> rotator) :
        filter2_m_{filter2_m},
        filter70_cm_{filter70_cm}, rotator_{rotator} {}

    /**
     * @brief Gets filter 2m state
     * @return optional filter 2m state
     */
    std::optional<bool> get_filter2_m() const { return filter2_m_; }

    /**
     * @brief Sets filter 2m state
     * @param value filter 2m state
     */
    void set_filter2_m(std::optional<bool> value) { filter2_m_ = value; }

    /**
     * @brief Gets filter 70cm state
     * @return optional filter 70cm state
     */
    std::optional<bool> get_filter70_cm() const { return filter70_cm_; }

    /**
     * @brief Sets filter 70cm state
     * @param value filter 70cm state
     */
    void set_filter70_cm(std::optional<bool> value) { filter70_cm_ = value; }

    /**
     * @brief Gets rotator state
     * @return optional rotator state
     */
    std::optional<bool> get_rotator() const { return rotator_; }

    /**
     * @brief Sets rotator state
     * @param value rotator state
     */
    void set_rotator(std::optional<bool> value) { rotator_ = value; }

private:
    std::optional<bool> filter2_m_;
    std::optional<bool> filter70_cm_;
    std::optional<bool> rotator_;
};

/** @} */

} // namespace gcs::frames

namespace nlohmann {
void from_json(const json& j, gcs_frames::set_relays& x);
void to_json(json& j, const gcs_frames::set_relays& x);

inline void from_json(const json& j, gcs_frames::set_relays& x) {
    x.set_filter2_m(gcs::frames::get_optional<bool>(j, "filter2m"));
    x.set_filter70_cm(gcs::frames::get_optional<bool>(j, "filter70cm"));
    x.set_rotator(gcs::frames::get_optional<bool>(j, "rotator"));
}

inline void to_json(json& j, const gcs_frames::set_relays& x) {
    j = json::object();
    if (x.get_filter2_m().has_value()) {
        j["filter2m"] = x.get_filter2_m().value();
    }
    if (x.get_filter70_cm().has_value()) {
        j["filter70cm"] = x.get_filter70_cm().value();
    }
    if (x.get_rotator().has_value()) {
        j["rotator"] = x.get_rotator().value();
    }
}
} // namespace nlohmann
