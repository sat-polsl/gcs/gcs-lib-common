#pragma once

#include <nlohmann/json.hpp>

#include <optional>
#include <stdexcept>
#include <cstdint>
#include "gcs/frames/nlohmann.h"

namespace gcs::frames {
/**
 * @ingroup gcs_frames
 * @{
 */

/**
 * @brief RX Frame frame
 */
class rx_frame {
public:
    /**
     * @brief Default constructor.
     */
    rx_frame() = default;

    /**
     * @brief Constructor.
     * @param altitude optional altitude
     * @param latitude optional latitude
     * @param longitude optional longitude
     * @param spacecraft_id spacecraft id
     * @param timestamp UNIX timestamp
     */
    rx_frame(std::optional<double> altitude, std::optional<double> latitude,
             std::optional<double> longitude, std::uint32_t spacecraft_id,
             std::uint32_t timestamp) :
        altitude_{altitude},
        latitude_{latitude}, longitude_{longitude}, spacecraft_id_{spacecraft_id}, timestamp_{
                                                                                       timestamp} {}

    /**
     * @brief Gets altitude
     * @return optional latitude
     */
    std::optional<double> get_altitude() const { return altitude_; }

    /**
     * @brief Sets altitude
     * @param value altitude
     */
    void set_altitude(std::optional<double> value) { altitude_ = value; }

    /**
     * @brief Gets latitude
     * @return optional latitude
     */
    std::optional<double> get_latitude() const { return latitude_; }

    /**
     * @brief Sets latitude
     * @param value latitude
     */
    void set_latitude(std::optional<double> value) { latitude_ = value; }

    /**
     * @brief Gets longitude
     * @return optional longitude
     */
    std::optional<double> get_longitude() const { return longitude_; }

    /**
     * @brief Sets latitude
     * @param value longitude
     */
    void set_longitude(std::optional<double> value) { longitude_ = value; }

    /**
     * @brief Gets spacecraft id
     * @return spacecraft id
     */
    std::uint32_t get_spacecraft_id() const { return spacecraft_id_; }

    /**
     * @brief Sets spacecraft id
     * @param value spacecraft id
     */
    void set_spacecraft_id(const std::uint32_t& value) { spacecraft_id_ = value; }

    /**
     * @brief Gets UNIX timestamp
     * @return UNIX timestamp
     */
    std::uint32_t get_timestamp() const { return timestamp_; }

    /**
     * @brief Sets UNIX timestamp
     * @param value UNIX timestamp
     */
    void set_timestamp(const std::uint32_t& value) { timestamp_ = value; }

private:
    std::optional<double> altitude_;
    std::optional<double> latitude_;
    std::optional<double> longitude_;
    std::uint32_t spacecraft_id_;
    std::uint32_t timestamp_;
};

/** @} */
} // namespace gcs::frames

namespace nlohmann {
void from_json(const json& j, gcs_frames::rx_frame& x);
void to_json(json& j, const gcs_frames::rx_frame& x);

inline void from_json(const json& j, gcs_frames::rx_frame& x) {
    x.set_altitude(gcs::frames::get_optional<double>(j, "altitude"));
    x.set_latitude(gcs::frames::get_optional<double>(j, "latitude"));
    x.set_longitude(gcs::frames::get_optional<double>(j, "longitude"));
    x.set_spacecraft_id(j.at("spacecraft_id").get<std::uint32_t>());
    x.set_timestamp(j.at("timestamp").get<std::uint32_t>());
}

inline void to_json(json& j, const gcs_frames::rx_frame& x) {
    j = json::object();
    if (x.get_altitude().has_value()) {
        j["altitude"] = x.get_altitude().value();
    }
    if (x.get_latitude().has_value()) {
        j["latitude"] = x.get_latitude().value();
    }
    if (x.get_longitude().has_value()) {
        j["longitude"] = x.get_longitude().value();
    }
    j["spacecraft_id"] = x.get_spacecraft_id();
    j["timestamp"] = x.get_timestamp();
}
} // namespace nlohmann
