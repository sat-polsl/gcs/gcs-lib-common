set(TARGET gcs_socket)

add_library(${TARGET} STATIC)
add_library(gcs::socket ALIAS gcs_socket)

target_sources(${TARGET}
    PRIVATE
    src/socket.cpp
    src/context.cpp
    )

target_include_directories(${TARGET}
    PUBLIC
    include
    )

target_link_libraries(${TARGET}
    PUBLIC
    libzmq-static
    cppzmq-static
    )
