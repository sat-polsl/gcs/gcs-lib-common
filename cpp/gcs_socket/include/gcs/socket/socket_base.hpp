#pragma once

#include <chrono>
#include <optional>
#include <string>

namespace gcs::socket {

/**
 * @ingroup gcs_socket
 * @{
 */

/**
 * @brief Message struct.
 */
struct message {
  std::string topic;
  std::string data;
};

/**
 * @brief Socket base.
 */
class socket_base {
 public:
  /**
   * @brief Starts receive operation. On success returns received message.
   * @return Message on success, nullopt on failure.
   */
  virtual std::optional<message> receive() = 0;

  /**
   * @brief Sends message.
   * @param msg Message to send.
   * @return True on succes, false otherwise.
   */
  virtual bool send(const message& msg) = 0;

  /**
   * @brief Subscribes topic.
   * @param topic Topic to subscribe.
   */
  virtual void subscribe(const std::string& topic) = 0;

  /**
   * @brief Sets receive timeout.
   * @param timeout Receive timeout.
   */
  virtual void set_receive_timeout(std::chrono::milliseconds timeout) = 0;

  /**
   * @brief Closes socket.
   */
  virtual void close() = 0;

  virtual ~socket_base() = default;
};

/** @} */

}  // namespace gcs::socket
