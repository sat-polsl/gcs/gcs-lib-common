#pragma once

#include "zmq.hpp"

namespace gcs::socket {

/**
 * @ingroup gcs_socket
 * @{
 */

/**
 * @brief Returns ZMQ socket.
 * @return ZMQ socket.
 */
zmq::context_t& context();

/** @} */
}
