include(FetchContent)

set(BUILD_SHARED OFF CACHE BOOL "")
set(BUILD_STATIC ON CACHE BOOL "")
set(ZMQ_BUILD_TESTS OFF CACHE BOOL "")
set(WITH_PERF_TOOL OFF CACHE BOOL "")
set(ENABLE_CPACK OFF CACHE BOOL "")

FetchContent_Declare(
        zmq
        GIT_REPOSITORY https://github.com/zeromq/libzmq.git
        GIT_TAG master
)

FetchContent_MakeAvailable(zmq)
