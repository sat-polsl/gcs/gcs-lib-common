GO ?= go

.PHONY: clean
clean:
	rm -rf go/zmq/*.pb.go
	rm -rf go/grpc/*.pb.go

.PHONY: go
go: clean
	./scripts/generator-go.sh

.PHONY: cpp
cpp:
	./scripts/generator-cpp.sh

.PHONY: e2e
e2e:
	TEST_MODE="-tags=e2e" make test

.PHONY: test
test:
	${GO} test ${TEST_MODE} \
		-cover \
		-race \
		-covermode=atomic \
		-coverprofile=coverage.out \
		./...

.PHONY: quicktype
quicktype:
	docker build \
		--tag=quicktype \
		.
