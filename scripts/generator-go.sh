#!/bin/bash -aex

function zeromq {
    for f in ./json/*; do
        file=$(basename "${f}")

        gojsonschema \
            --package=frames \
            --output=go/zmq/"${file%.*}".go \
            json/"${file}"
    done
}

function grpc {
    for f in ./proto/*; do
        file=$(basename "${f}")

        protoc \
            --proto_path=proto \
            --go_out=go/grpc \
            --go_opt=paths=source_relative \
            --go-grpc_out=go/grpc \
            --go-grpc_opt=paths=source_relative \
            "${file}"
    done
}

function openapi {
    go generate ./...
}

zeromq
grpc
openapi
