#!/bin/bash -aex

go install github.com/deepmap/oapi-codegen/cmd/oapi-codegen@latest
go install github.com/atombender/go-jsonschema/cmd/gojsonschema@latest
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2
